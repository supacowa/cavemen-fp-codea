-- Adjust brush position when it goes outside the spritesheet bounds

function world:pullBrushIntoAtlasBounds()
    local cols = self.atlas_texture.width / self.tile_width
    local rows = self.atlas_texture.height / self.tile_height
    local overlap_x = self.brush_x + self.brush_width - cols
    local overlap_y = self.brush_y + self.brush_height - rows
    
    if overlap_x > 0 then self.brush_x = self.brush_x - overlap_x end
    if overlap_y > 0 then self.brush_y = self.brush_y - overlap_y end
end












-- Adjust atlas_texture position when it goes outside the spritesheet window

function world:pullAtlasIntoAtlasWindowBounds()
    local atlas_width = self.atlas_texture.width * self.atlas_zoom_x
    
    if ( -- atlas_texture.width smaller than WIDTH?
        atlas_width < WIDTH
        and (self.atlas_x <= 0 or self.atlas_x >= WIDTH or self.atlas_x + atlas_width <= 0 or self.atlas_x + atlas_width >= WIDTH)
    )
    or ( -- atlas_texture.width larger than WIDTH?
        atlas_width > WIDTH
        and (self.atlas_x > 0 or self.atlas_x + atlas_width < WIDTH)
    )
    then
        self.atlas_x = 0
    end
end













-- Clamp the height of the spritesheet window

function world:pullAtlasWindowHeightOverflow()
    local window_height = HEIGHT * self.atlas_window_height
    local atlas_height = self.atlas_texture.height * self.atlas_zoom_y + self.title_bar_height
    
    if window_height > atlas_height then
        self.atlas_window_height = atlas_height / HEIGHT
    end
end